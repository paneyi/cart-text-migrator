const shops = require('./collection/shop')

const ALL_COLLECTIONS_WILDCARD = 'all'

const MIGRATE_HISTORY_METHODS = {
  [shops.FIREBASE_COLLECTION]: shops.fetchAndMigrate
}

const MIGRATE_LIVE_METHODS = {
  [shops.FIREBASE_COLLECTION]: shops.migrateLive
}

const VALID_COLLECTIONS = [shops.FIREBASE_COLLECTION]

module.exports = {
  ALL_COLLECTIONS_WILDCARD,
  MIGRATE_HISTORY_METHODS,
  MIGRATE_LIVE_METHODS,
  VALID_COLLECTIONS
}
