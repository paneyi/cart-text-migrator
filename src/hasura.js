const { GraphQLClient } = require('graphql-request')
const { HASURA_KEY, HASURA_HEADER, HASURA_URL } = require('./config')

const graphQLClient = new GraphQLClient(HASURA_URL, {
  headers: {
    [HASURA_HEADER]: HASURA_KEY
  }
})

module.exports = graphQLClient
