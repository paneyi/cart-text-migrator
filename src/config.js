const path = require('path')
const dotenv = require('dotenv')

const envPath = process.env.ENV_PATH || process.cwd()
const { parsed: config } = dotenv.config({ path: path.resolve(envPath, '.env') })

module.exports = config
