const admin = require('firebase-admin')
const { FIREBASE_DATABASE_URL, FIREBASE_KEY } = require('./config')

const certificate = JSON.parse(Buffer.from(FIREBASE_KEY, 'base64').toString())

admin.initializeApp({
  credential: admin.credential.cert(certificate),
  databaseURL: FIREBASE_DATABASE_URL
})

module.exports = admin
