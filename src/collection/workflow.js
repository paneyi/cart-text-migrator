const _ = require('lodash')
const { gql } = require('graphql-request')
const hasura = require('../hasura')

const build = ({ document, shopIntegrations, shopId }) => {
  const WORKFLOW_KEYS = [
    'checkout_recovery',
    'custom_recovery',
    'order_reminder',
    'order_upsell',
    'subscription_cancellation_recovery',
    'subscription_billing_recovery'
  ]

  const { settings } = document

  const workflows = WORKFLOW_KEYS.reduce((workflows, key) => {
    const workflow = settings[key]
    if (workflow) {
      Object.values(shopIntegrations).forEach((integration) => {
        if (!workflows[integration.platform])
          workflows[integration.platform] = {}

        workflows[integration.platform][key] = {
          name: _.capitalize(
            `${integration.platform} ${key.replace(/_/g, ' ')}`
          ),
          description: '',
          shopId,
          shopIntegrationId: integration.id,
          type: key,
          platform: integration.platform,
          enabled: workflow.enabled,
          rules: { all: [] },
          config: {
            steps: []
          }
        }
      })
    }
    return workflows
  }, {})

  return workflows
}

const find = async ({ shopId }) => {
  const query = gql`
    query find($shopId: uuid) {
      workflows(where: { shop_id: { _eq: $shopId } }) {
        id,
        platform,
        type
      }
    }
  `
  const variables = {
    shopId
  }
  const { workflows } = await hasura.request(query, variables)

  const formattedWorkflows = {}
  workflows.forEach((workflow) => {
    if (!formattedWorkflows[workflow.platform])
      formattedWorkflows[workflow.platform] = {}
    formattedWorkflows[workflow.platform][workflow.type] = workflow
  })
  return formattedWorkflows
}

const findOrCreate = async ({ document, shopId, shopIntegrations }) => {
  const currentWorkflows = await find({ shopId })
  const incomingWorkflows = build({ document, shopId, shopIntegrations })

  const workflowsToInsert = []
  for (const [platform, typedWorkflow] of Object.entries(incomingWorkflows)) {
    for (const [type, workflow] of Object.entries(typedWorkflow)) {
      if (!currentWorkflows[platform] || !currentWorkflows[platform][type]) workflowsToInsert.push(workflow)
    }
  }

  const insertedWorkflows = {}
  if (workflowsToInsert.length) {
    const newWorkflows = await insert({
      workflows: workflowsToInsert
    })
    newWorkflows.forEach((workflow) => {
      if (!insertedWorkflows[workflow.platform])
      insertedWorkflows[workflow.platform] = {}
      insertedWorkflows[workflow.platform][workflow.type] = workflow
    })
  }

  return { ...currentWorkflows, ...insertedWorkflows }
}

const insert = async ({ workflows }) => {
  const mutation = gql`
    mutation insert_workflows($objects: [workflows_insert_input!]!) {
      insert_workflows(objects: $objects) {
        returning {
          id,
          platform
          type
        }
      }
    }
  `
  const variables = {
    objects: workflows.map((workflow) => ({
      config: workflow.config,
      description: workflow.description,
      enabled: workflow.enabled,
      name: workflow.name,
      platform: workflow.platform,
      rules: workflow.rules,
      shop_id: workflow.shopId,
      shop_integration_id: workflow.shopIntegrationId,
      type: workflow.type
    }))
  }
  const {
    insert_workflows: { returning: results }
  } = await hasura.request(mutation, variables)
  return results
}

module.exports = {
  build,
  find,
  findOrCreate,
  insert
}
