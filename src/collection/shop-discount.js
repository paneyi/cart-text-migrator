const { gql } = require('graphql-request')
const hasura = require('../hasura')

const build = ({ document, shopId }) => {
  const BUILD_METHODS = {
    recharge: buildRechargeDiscount,
    shopify: buildShopifyDiscount
  }

  const {
    settings: {
      checkout_recovery: checkoutRecovery,
      custom_recovery: customRecovery,
      order_reminder: orderReminder,
      order_upsell: orderUpsell,
      subscription_billing_recovery: subscriptionBillingRecovery,
      subscription_cancellation_recovery: subscriptionCancellationRecovery
    }
  } = document

  const rawDiscounts = [
    ...(checkoutRecovery ? checkoutRecovery.discounts : []),
    ...(customRecovery ? customRecovery.discounts : []),
    ...(orderReminder ? orderReminder.discounts : []),
    ...(orderUpsell ? orderUpsell.discounts : []),
    ...(subscriptionBillingRecovery
      ? subscriptionBillingRecovery.discounts
      : []),
    ...(subscriptionCancellationRecovery
      ? subscriptionCancellationRecovery.discounts
      : [])
  ]

  const discounts = rawDiscounts.reduce((discounts, rawDiscount) => {
    const existingDiscount = discounts.find((discount) =>
      checkDiscountsEquality({
        firstDiscount: discount,
        secondDiscount: rawDiscount
      })
    )
    if (!existingDiscount) {
      discounts.push({
        shopId,
        ...BUILD_METHODS[rawDiscount.platform]({ rawDiscount })
      })
    }

    return discounts
  }, [])

  return discounts
}

const buildRechargeDiscount = ({ rawDiscount }) => {
  return {
    code: rawDiscount.code,
    createdAt: rawDiscount.created_at && new Date(rawDiscount.created_at),
    platform: rawDiscount.platform,
    type: rawDiscount.discount_type,
    updatedAt: rawDiscount.updated_at && new Date(rawDiscount.updated_at),
    value: String(Math.abs(rawDiscount.value))
  }
}

const buildShopifyDiscount = ({ rawDiscount }) => {
  const discount = {
    code: rawDiscount.code,
    createdAt: rawDiscount.created_at && new Date(rawDiscount.created_at),
    platform: rawDiscount.platform,
    type: rawDiscount.value_type,
    updatedAt: rawDiscount.updated_at && new Date(rawDiscount.updated_at),
    value: String(Math.abs(rawDiscount.value))
  }

  if (rawDiscount.target_type === 'shipping_line') discount.type = 'shipping'

  return discount
}

const checkDiscountsEquality = ({ firstDiscount, secondDiscount }) => {
  return (
    firstDiscount.code === secondDiscount.code &&
    firstDiscount.platform === secondDiscount.platform
  )
}

const find = async ({ shopId }) => {
  const query = gql`
    query find($shopId: uuid) {
      shop_discounts(where: { shop_id: { _eq: $shopId } }) {
        code
        id
        platform
        type
        value
      }
    }
  `
  const variables = {
    shopId
  }
  const { shop_discounts: shopDiscounts } = await hasura.request(
    query,
    variables
  )
  return shopDiscounts
}

const findOrCreate = async ({ document, shopId }) => {
  const currentShopDiscounts = await find({ shopId })
  const incomingShopDiscounts = build({ document, shopId })

  const discountsToInsert = incomingShopDiscounts.filter(
    (incomingDiscount) =>
      !currentShopDiscounts.some((currentDiscount) =>
        checkDiscountsEquality({
          firstDiscount: currentDiscount,
          secondDiscount: incomingDiscount
        })
      )
  )

  const insertedShopDiscounts = []
  if (discountsToInsert.length) {
    insertedShopDiscounts.push(
      ...(await insert({ discounts: discountsToInsert }))
    )
  }

  return [...currentShopDiscounts, ...insertedShopDiscounts].map(
    (discount) => discount.id
  )
}

const insert = async ({ discounts }) => {
  const mutation = gql`
    mutation insert_shop_discounts($objects: [shop_discounts_insert_input!]!) {
      insert_shop_discounts(objects: $objects) {
        returning {
          id
        }
      }
    }
  `
  const variables = {
    objects: discounts.map((discount) => ({
      code: discount.code,
      created_at: discount.createdAt,
      platform: discount.platform,
      shop_id: discount.shopId,
      type: discount.type,
      updated_at: discount.updatedAt,
      value: discount.value
    }))
  }
  const {
    insert_shop_discounts: { returning: results }
  } = await hasura.request(mutation, variables)
  return results
}

module.exports = {
  build,
  find,
  findOrCreate,
  insert
}
