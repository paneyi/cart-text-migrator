const { gql } = require('graphql-request')
const hasura = require('../hasura')

const build = ({ document, shopId }) => {
  const INTEGRATION_KEYS = ['recharge', 'shopify']

  const { shop } = document

  const integrations = INTEGRATION_KEYS.reduce((integrations, key) => {
    const integration = shop[key]
    if (integration) {
      integrations[integration.platform] = {
        accessToken: integration.access_token,
        apiVersion: integration.api_version,
        enabled: integration.enabled,
        name: `${integration.platform
          .charAt(0)
          .toUpperCase()}${integration.platform.slice(1)}`,
        platform: integration.platform,
        shopId,
        webhooks: integration.webhooks
      }
    }
    return integrations
  }, {})

  return integrations
}

const find = async ({ shopId }) => {
  const query = gql`
    query find($shopId: uuid) {
      shop_integrations(where: { shop_id: { _eq: $shopId } }) {
        id
        platform
      }
    }
  `
  const variables = {
    shopId
  }
  const { shop_integrations: shopIntegrations } = await hasura.request(
    query,
    variables
  )

  const formattedIntegrations = {}
  shopIntegrations.forEach((integration) => {
    formattedIntegrations[integration.platform] = integration
  })
  return formattedIntegrations
}

const findOrCreate = async ({ document, shopId }) => {
  const currentShopIntegrations = await find({ shopId })
  const incomingShopIntegrations = build({ document, shopId })

  const integrationsToInsert = []
  for (const [platform, integration] of Object.entries(
    incomingShopIntegrations
  )) {
    if (!currentShopIntegrations[platform])
      integrationsToInsert.push(integration)
  }

  const insertedShopIntegrations = {}
  if (integrationsToInsert.length) {
    const newIntegrations = await insert({
      integrations: integrationsToInsert
    })
    newIntegrations.forEach((integration) => {
      insertedShopIntegrations[integration.platform] = integration
    })
  }

  return { ...currentShopIntegrations, ...insertedShopIntegrations }
}

const insert = async ({ integrations }) => {
  const mutation = gql`
    mutation insert_shop_integrations(
      $objects: [shop_integrations_insert_input!]!
    ) {
      insert_shop_integrations(objects: $objects) {
        returning {
          id
          platform
        }
      }
    }
  `
  const variables = {
    objects: integrations.map((integration) => ({
      access_token: integration.accessToken,
      api_version: integration.apiVersion,
      enabled: integration.enabled,
      name: integration.name,
      platform: integration.platform,
      shop_id: integration.shopId,
      webhooks: integration.webhooks
    }))
  }
  const {
    insert_shop_integrations: { returning: results }
  } = await hasura.request(mutation, variables)
  return results
}

module.exports = {
  build,
  find,
  findOrCreate,
  insert
}
