const { gql } = require('graphql-request')
const hasura = require('../hasura')

const build = ({ document, shopId }) => {
  return {
    active: document.billing.active,
    billingDay: document.billing.billing_day,
    information: document.billing.information,
    monthlyCharge: document.billing.monthly_charge,
    paymentProfileId: document.billing.payment_profile_id,
    perChargeType: document.billing.per_charge_type,
    perConversationAttribution: document.billing.per_conversation_attribution,
    perConversationCharge: document.billing.per_conversation_charge,
    perRecoveredOrderPercentage:
      document.billing.per_recovered_order_percentage,
    perRecoveredSubscriptionPercentage:
      document.billing.per_recovered_subscription_percentage,
    perRecoveredUpsellPercentage:
      document.billing.per_recovered_upsell_percentage,
    perRecoveryAttribution: document.billing.per_recovery_attribution,
    shopId,
    vaultId: document.billing.vault_id
  }
}

const find = async ({ shopId }) => {
  const query = gql`
    query find($shopId: uuid) {
      shop_billing(where: { shop_id: { _eq: $shopId } }) {
        id
      }
    }
  `
  const variables = {
    shopId
  }
  const { shop_billing: shopBillings } = await hasura.request(query, variables)
  return shopBillings
}

const findOrCreate = async ({ document, shopId }) => {
  let [shopBilling] = await find({ shopId })

  if (!shopBilling) {
    shopBilling = await insertOne(build({ document, shopId }))
  }
  return shopBilling
}

const insertOne = async ({
  active,
  billingDay,
  information,
  monthlyCharge,
  paymentProfileId,
  perChargeType,
  perConversationAttribution,
  perConversationCharge,
  perRecoveredOrderPercentage,
  perRecoveredSubscriptionPercentage,
  perRecoveredUpsellPercentage,
  perRecoveryAttribution,
  shopId,
  vaultId
}) => {
  const mutation = gql`
    mutation insert_single_shop_billing($object: shop_billing_insert_input!) {
      insert_shop_billing_one(object: $object) {
        id
      }
    }
  `
  const variables = {
    object: {
      active,
      billing_day: billingDay,
      information,
      monthly_charge: monthlyCharge,
      payment_profile_id: paymentProfileId,
      per_charge_type: perChargeType,
      per_conversation_attribution: perConversationAttribution,
      per_conversation_charge: perConversationCharge,
      per_recovered_order_percentage: perRecoveredOrderPercentage,
      per_recovered_subscription_percentage: perRecoveredSubscriptionPercentage,
      per_recovered_upsell_percentage: perRecoveredUpsellPercentage,
      per_recovery_attribution: perRecoveryAttribution,
      shop_id: shopId,
      vault_id: vaultId
    }
  }
  const { insert_shop_billing_one: result } = await hasura.request(
    mutation,
    variables
  )
  return result
}

module.exports = {
  build,
  find,
  findOrCreate,
  insertOne
}
