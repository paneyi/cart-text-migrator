const { gql } = require('graphql-request')
const hasura = require('../hasura')

const FIREBASE_COLLECTION = 'customers'
const EXTERNAL_CUSTOMER_ID_KEYS = {
  shopify: 'shopify_customer_id',
  recharge: 'customer_id'
}
const build = ({ document, shopId }) => {
  return {
    createdAt: document.created && new Date(document.created),
    customerName: document.customer_name,
    email: document.email,
    externalCustomerId:
      document[EXTERNAL_CUSTOMER_ID_KEYS[document.platform]] &&
      document[EXTERNAL_CUSTOMER_ID_KEYS[document.platform]].toString(),
    firstName: document.first_name,
    lastName: document.last_name,
    phone: document.phone,
    platform: document.platform,
    shopId,
    state: document.state,
    timezone: document.timezone,
    updatedAt: document.updated && new Date(document.updated),
    vault: document.vault
  }
}

const fetchAndMigrate = async ({
  customData,
  firebaseCustomerId,
  firebaseShopRef,
  shopId
} = {}) => {
  const collection = firebaseShopRef.collection(FIREBASE_COLLECTION)
  const document = await collection.doc(firebaseCustomerId).get()

  if (document.exists)
    return migrate({ customData, firebaseDoc: document, shopId })
  if (customData) return findOrCreate({ document: customData, shopId })

  throw { message: 'NOT_FOUND', payload: 'CUSTOMER' }
}

const find = async ({ phone, shopId }) => {
  const query = gql`
    query find($phone: String, $shopId: uuid) {
      customers(where: { phone: { _eq: $phone }, shop_id: { _eq: $shopId } }) {
        id
      }
    }
  `
  const variables = {
    phone,
    shopId
  }
  const { customers } = await hasura.request(query, variables)
  return customers
}

const findOrCreate = async ({ document, shopId }) => {
  let [customer] = await find({ phone: document.phone, shopId })
  if (!customer) {
    customer = await insertOne(build({ document, shopId }))
  }
  return customer
}

const insertOne = async ({
  createdAt,
  customerName,
  email,
  externalCustomerId,
  firstName,
  lastName,
  phone,
  platform,
  shopId,
  state,
  timezone,
  updatedAt,
  vault
}) => {
  const mutation = gql`
    mutation insert_single_customer($object: customers_insert_input!) {
      insert_customers_one(object: $object) {
        id
      }
    }
  `
  const variables = {
    object: {
      created_at: createdAt,
      customer_name: customerName,
      email,
      external_customer_id: externalCustomerId,
      first_name: firstName,
      last_name: lastName,
      phone,
      platform,
      shop_id: shopId,
      state,
      timezone,
      updated_at: updatedAt,
      vault
    }
  }
  const { insert_customers_one: result } = await hasura.request(
    mutation,
    variables
  )
  return result
}

const migrate = async ({ customData, firebaseDoc, shopId }) => {
  const document = {
    ...firebaseDoc.data(),
    ...customData,
    id: firebaseDoc.id
  }

  return findOrCreate({ document, shopId })
}

module.exports = {
  build,
  fetchAndMigrate,
  find,
  findOrCreate,
  insertOne,
  migrate
}
