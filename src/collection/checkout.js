const { gql } = require('graphql-request')
const hasura = require('../hasura')

const FIREBASE_COLLECTION = 'checkouts'

const build = ({ customerId, document, shopId }) => {
  return {
    abandonedCheckoutUrl:
      document.abandoned_checkout_url || document.checkout_url,
    cartToken: document.cart_token,
    createdAt: document.created && new Date(document.created),
    customerId,
    discountCodes: document.discount_codes,
    email: document.email,
    externalCustomer: document.customer,
    externalId: document.external_id,
    lineItems: document.line_items,
    note: document.note,
    noteAttributes: document.note_attributes,
    phone: document.phone,
    platform: document.platform,
    shippingAddress: document.shipping_address,
    shopId,
    token: document.token,
    totalDiscounts: document.total_discounts,
    totalPrice: document.total_price,
    updatedAt: document.updated && new Date(document.updated)
  }
}

const fetchAndMigrate = async ({
  customData,
  customerId,
  firebaseCheckoutId,
  firebaseShopRef,
  shopId
} = {}) => {
  const collection = firebaseShopRef.collection(FIREBASE_COLLECTION)
  const document = await collection.doc(firebaseCheckoutId).get()

  if (document.exists)
    return migrate({ customData, customerId, firebaseDoc: document, shopId })
  if (customData)
    return findOrCreate({ customerId, document: customData, shopId })

  throw { message: 'NOT_FOUND', payload: 'CHECKOUT' }
}

const find = async ({ customerId, externalId }) => {
  const query = gql`
    query find($customerId: uuid, $externalId: String) {
      checkouts(
        where: {
          customer_id: { _eq: $customerId }
          external_id: { _eq: $externalId }
        }
      ) {
        id
      }
    }
  `
  const variables = {
    customerId,
    externalId
  }
  const { checkouts } = await hasura.request(query, variables)
  return checkouts
}

const findOrCreate = async ({ customerId, document, shopId }) => {
  const formattedCheckout = build({ customerId, document, shopId })
  let [checkout] = await find({
    customerId,
    externalId: formattedCheckout.externalId
  })
  if (!checkout) {
    checkout = await insertOne(formattedCheckout)
  }
  return checkout
}

const insertOne = async ({
  abandonedCheckoutUrl,
  cartToken,
  createdAt,
  customerId,
  customerName,
  discountCodes,
  email,
  externalCustomer,
  externalId,
  lineItems,
  note,
  noteAttributes,
  phone,
  platform,
  shippingAddress,
  shopId,
  token,
  totalDiscounts,
  totalPrice,
  updatedAt
}) => {
  const mutation = gql`
    mutation insert_single_checkout($object: checkouts_insert_input!) {
      insert_checkouts_one(object: $object) {
        id
      }
    }
  `
  const variables = {
    object: {
      abandoned_checkout_url: abandonedCheckoutUrl,
      cart_token: cartToken,
      created_at: createdAt,
      customer_id: customerId,
      customer_name: customerName,
      discount_codes: discountCodes,
      email,
      external_customer: externalCustomer,
      external_id: externalId,
      line_items: lineItems,
      note,
      note_attributes: noteAttributes,
      phone,
      platform,
      shipping_address: shippingAddress,
      shop_id: shopId,
      token,
      total_discounts: totalDiscounts,
      total_price: totalPrice,
      updated_at: updatedAt
    }
  }
  const { insert_checkouts_one: result } = await hasura.request(
    mutation,
    variables
  )
  return result
}

const migrate = async ({ customData, customerId, firebaseDoc, shopId }) => {
  const document = {
    ...firebaseDoc.data(),
    ...customData,
    id: firebaseDoc.id
  }

  return findOrCreate({ customerId, document, shopId })
}

module.exports = {
  build,
  fetchAndMigrate,
  find,
  findOrCreate,
  insertOne,
  migrate
}
