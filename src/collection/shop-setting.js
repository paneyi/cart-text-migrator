const { gql } = require('graphql-request')
const hasura = require('../hasura')

const build = ({ document, shopId }) => {
  return {
    merchant: document.shop.merchant,
    redirectSettings: document.settings.redirect,
    recharge: document.shop.recharge,
    shopId,
    shopify: document.shop.shopify,
    twilio: document.twilio
  }
}

const find = async ({ shopId }) => {
  const query = gql`
    query find($shopId: uuid) {
      shop_settings(where: { shop_id: { _eq: $shopId } }) {
        id
      }
    }
  `
  const variables = {
    shopId
  }
  const { shop_settings: shopSettings } = await hasura.request(query, variables)
  return shopSettings
}

const findOrCreate = async ({ document, shopId }) => {
  let [shopSetting] = await find({ shopId })
  if (!shopSetting) {
    shopSetting = await insertOne(build({ document, shopId }))
  }
  return shopSetting
}

const insertOne = async ({
  merchant,
  redirectSettings,
  recharge,
  shopId,
  shopify,
  twilio
}) => {
  const mutation = gql`
    mutation insert_single_shop_setting($object: shop_settings_insert_input!) {
      insert_shop_settings_one(object: $object) {
        id
      }
    }
  `
  const variables = {
    object: {
      merchant,
      redirect_settings: redirectSettings,
      recharge,
      shop_id: shopId,
      shopify,
      twilio
    }
  }
  const { insert_shop_settings_one: result } = await hasura.request(
    mutation,
    variables
  )
  return result
}

module.exports = {
  build,
  find,
  findOrCreate,
  insertOne
}
