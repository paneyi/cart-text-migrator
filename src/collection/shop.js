const { gql } = require('graphql-request')
const { eachOfLimit } = require('async')
const firebase = require('../firebase')
const hasura = require('../hasura')

const { findOrCreate: findOrCreateAccount } = require('./account')
const { findOrCreate: findOrCreateBilling } = require('./shop-billing')
const { findOrCreate: findOrCreateDiscounts } = require('./shop-discount')
const { findOrCreate: findOrCreateIntegrations } = require('./shop-integration')
const { findOrCreate: findOrCreateSetting } = require('./shop-setting')
const { findOrCreate: findOrCreateWorkflows } = require('./workflow')

const FIREBASE_COLLECTION = 'shops'
const DOCUMENTS_PARALLEL_LIMIT = 1

const build = ({ accountId, document }) => {
  document = {
    ...document,
    shop: document.shop || {},
    twilio: document.twilio || {}
  }
  return {
    accountId,
    active: document.shop.active,
    channel: document.twilio.channel,
    cleanShopName: document.shop.shop
      ? document.shop.shop.split('.')[0]
      : undefined,
    createdAt: document.created && new Date(document.created),
    domain: document.shop.domain,
    email: document.email,
    notes: document.shop.notes,
    onboard: document.onboard,
    setup: document.setup,
    shop: document.shop.shop,
    shopEmail: document.shop.shop_email,
    shopName: document.shop.shop_name,
    shopPhone: document.shop.shop_phone,
    supportEmail: document.shop.support_email
  }
}

const find = async ({ shop }) => {
  const query = gql`
    query find($shop: String) {
      shops(where: { shop: { _eq: $shop } }) {
        id
      }
    }
  `
  const variables = {
    shop
  }
  const { shops } = await hasura.request(query, variables)
  return shops
}

const findOrCreate = async ({ accountId, document }) => {
  let [shop] = await find({ shop: document.shop.shop })
  if (!shop) {
    shop = await insertOne(build({ accountId, document }))
  }
  return shop
}

const insertOne = async ({
  accountId,
  active,
  channel,
  cleanShopName,
  createdAt,
  domain,
  email,
  notes,
  onboard,
  setup,
  shop,
  shopEmail,
  shopName,
  shopPhone,
  supportEmail
}) => {
  const mutation = gql`
    mutation insert_single_shop($object: shops_insert_input!) {
      insert_shops_one(object: $object) {
        id
      }
    }
  `
  const variables = {
    object: {
      account_id: accountId,
      active,
      channel,
      clean_shop_name: cleanShopName,
      created_at: createdAt,
      domain,
      email,
      notes,
      onboard,
      setup,
      shop,
      shop_email: shopEmail,
      shop_name: shopName,
      shop_phone: shopPhone,
      support_email: supportEmail
    }
  }
  const { insert_shops_one: result } = await hasura.request(mutation, variables)
  return result
}

const migrate = async ({ document }) => {
  if (!document.setup.complete) return

  try {
    const account = await findOrCreateAccount({ document })
    const shop = await findOrCreate({ accountId: account.id, document })
    const [_shopBilling, _shopSetting, _shopDiscounts, shopIntegrations] =
      await Promise.all([
        findOrCreateBilling({ document, shopId: shop.id }),
        findOrCreateSetting({ document, shopId: shop.id }),
        findOrCreateDiscounts({ document, shopId: shop.id }),
        findOrCreateIntegrations({ document, shopId: shop.id })
      ])
    await findOrCreateWorkflows({ document, shopId: shop.id, shopIntegrations })
  } catch (err) {
    console.log(err)
  }
}

const migrateHistory = async () => {
  const db = firebase.firestore()
  const collection = db.collection(FIREBASE_COLLECTION)
  const snapshot = await collection.where('setup.complete', '==', true).get()

  // ToDo: remove slice when development is done.
  const docs = snapshot.docs.slice(0, 1)
  await eachOfLimit(docs, DOCUMENTS_PARALLEL_LIMIT, async (doc) => {
    return migrate({
      document: {
        id: doc.id,
        ...doc.data()
      }
    })
  })
}

const migrateLive = async () => {
  const db = firebase.firestore()
  const collection = db.collection(FIREBASE_COLLECTION)
  const query = await collection.where('setup.complete', '==', true)

  const unsubscribe = query.onSnapshot(async (querySnapshot) => {
    for (const change of querySnapshot.docChanges()) {
      if (change.type === 'added') {
        const document = {
          id: change.doc.id,
          ...change.doc.data()
        }
        console.log(document.id)
        // await migrate({ document })
      }
      if (change.type === 'modified') {
        console.log('Modified shop: ', change.doc.data())
        // ToDo: Update is a bit trickier.
      }
      if (change.type === 'removed') {
        console.log('Removed shop: ', change.doc.data())
        // ToDo: Remove shop and all components.
      }
    }
  })

  return unsubscribe
}

module.exports = {
  build,
  find,
  findOrCreate,
  FIREBASE_COLLECTION,
  insertOne,
  migrateHistory,
  migrateLive
}
