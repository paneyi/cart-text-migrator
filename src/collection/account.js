const { gql } = require('graphql-request')
const hasura = require('../hasura')

const build = ({ document }) => {
  return {
    uid: document.id,
    email: document.email
  }
}

const find = async ({ uid }) => {
  const query = gql`
    query find($uid: String) {
      accounts(where: { uid: { _eq: $uid } }) {
        id
      }
    }
  `
  const variables = {
    uid
  }
  const { accounts } = await hasura.request(query, variables)
  return accounts
}

const findOrCreate = async ({ document }) => {
  let [account] = await find({ uid: document.id })
  if (!account) {
    account = await insertOne(build({ document }))
  }
  return account
}

const insertOne = async ({ uid, email }) => {
  const mutation = gql`
    mutation insert_single_account($object: accounts_insert_input!) {
      insert_accounts_one(object: $object) {
        id
      }
    }
  `
  const variables = {
    object: {
      uid,
      email
    }
  }
  const { insert_accounts_one: result } = await hasura.request(
    mutation,
    variables
  )
  return result
}

module.exports = {
  build,
  find,
  findOrCreate,
  insertOne
}
