const { gql } = require('graphql-request')
const hasura = require('../hasura')

const build = ({ customerId, document, shopId }) => {
  const subscriptions = document.subscriptions.map((subscription) => {
    return {
      createdAt: subscription.created_at && new Date(subscription.created_at),
      customerId,
      email: subscription.email,
      externalId: subscription.id && subscription.id.toString(),
      externalCustomerId: subscription.customer_id && subscription.customer_id.toString(),
      phone: subscription.phone,
      platform: document.platform,
      shopId,
      updatedAt: subscription.updated_at && new Date(subscription.updated_at)
    }
  })
  return subscriptions
}

const checkSubscriptionsEquality = ({
  firstSubscription,
  secondSubscription
}) => {
  return (
    firstSubscription.customerId === secondSubscription.customerId &&
    firstSubscription.externalId === secondSubscription.externalId
  )
}

const find = async ({ customerId }) => {
  const query = gql`
    query find($customerId: uuid) {
      subscriptions(where: { customer_id: { _eq: $customerId } }) {
        customer_id
        external_id
        id
      }
    }
  `
  const variables = {
    customerId
  }
  const { subscriptions } = await hasura.request(query, variables)
  return subscriptions
}

const findOrCreate = async ({ customerId, document, shopId }) => {
  const incomingSubscriptions = build({ customerId, document, shopId })
  const currentSubscriptions = await find({ customerId })

  const subscriptionsToInsert = incomingSubscriptions.filter(
    (incomingSubscription) =>
      !currentSubscriptions.some((currentSubscription) =>
        checkSubscriptionsEquality({
          firstSubscription: currentSubscription,
          secondSubscription: incomingSubscription
        })
      )
  )

  const insertedSubscriptions = []
  if (subscriptionsToInsert.length) {
    insertedSubscriptions.push(
      ...(await insert({ subscriptions: subscriptionsToInsert }))
    )
  }

  return [...currentSubscriptions, ...insertedSubscriptions].map(
    (subscription) => subscription.id
  )
}

const insert = async ({ subscriptions }) => {
  const mutation = gql`
    mutation insert_subscriptions($objects: [subscriptions_insert_input!]!) {
      insert_subscriptions(objects: $objects) {
        returning {
          id
        }
      }
    }
  `
  const variables = {
    objects: subscriptions.map((subscription) => ({
      created_at: subscription.createdAt,
      customer_id: subscription.customerId,
      email: subscription.email,
      external_id: subscription.externalId,
      external_customer_id: subscription.externalCustomerId,
      phone: subscription.phone,
      platform: subscription.platform,
      shop_id: subscription.shopId,
      updated_at: subscription.updatedAt
    }))
  }
  const {
    insert_subscriptions: { returning: results }
  } = await hasura.request(mutation, variables)
  return results
}

module.exports = {
  build,
  find,
  findOrCreate,
  insert
}
