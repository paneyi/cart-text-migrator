#!/usr/bin/env node

const { program } = require('commander')
const packageJson = require('../package.json')
const {
  ALL_COLLECTIONS_WILDCARD,
  MIGRATE_HISTORY_METHODS,
  MIGRATE_LIVE_METHODS,
  VALID_COLLECTIONS
} = require('../src/utils')

const collectionsParser = (values) => {
  const collections = values.split(',').map((value) => value && value.trim())

  const wildcardProvided = collections[0] === ALL_COLLECTIONS_WILDCARD
  return wildcardProvided
    ? VALID_COLLECTIONS
    : [
        ...new Set(
          collections.filter((collection) =>
            VALID_COLLECTIONS.includes(collection)
          )
        )
      ]
}

program
  .version(packageJson.version)
  .name('migrator')
  .usage('[options]')
  .option('-d, --debug', 'show additional info')
  .requiredOption(
    '-c, --collections <collections>',
    `comma-separeted list of Firestore collections to migrate. 'all' to migrate every collection. Valid collections are [${VALID_COLLECTIONS}]`,
    collectionsParser
  )

program
  .command('history')
  .description('migrate current data from collections')
  .action(async () => {
    try {
      const { collections = [], debug = false } = program.opts()

      if (!collections.length) {
        throw {
          message: 'EMPTY',
          payload: 'MIGRATOR.COLLECTIONS'
        }
      }

      if (debug) {
        console.log(
          `[migrator-history] Collections to migrate: ${collections}.`
        )
      }

      for (const collectionName of collections) {
        if (debug) {
          console.log(
            `[migrator-history] ${new Date()}: Start migrating ${collectionName}.`
          )
        }

        await MIGRATE_HISTORY_METHODS[collectionName]()

        if (debug) {
          console.log(
            `[migrator-history] ${new Date()}: Finish migrating ${collectionName}.`
          )
        }
      }
    } catch (error) {
      console.error(
        `[migrator-history] Error while running history migrator script`,
        error
      )
    }

    process.exit(0)
  })

program
  .command('live')
  .description(`listen for events and migrate new collection's docs`)
  .action(async () => {
    try {
      const { collections = [], debug = false } = program.opts()

      const wildcardProvided = collections[0] === ALL_COLLECTIONS_WILDCARD
      const validCollections = wildcardProvided
        ? VALID_COLLECTIONS
        : [
            ...new Set(
              collections.filter((collection) =>
                VALID_COLLECTIONS.includes(collection)
              )
            )
          ]

      if (!validCollections.length) {
        throw {
          message: 'EMPTY',
          payload: 'MIGRATOR.COLLECTIONS'
        }
      }

      if (debug) {
        if (wildcardProvided)
          console.log(
            `[migrator-live] Wildcard provided, about to migrate all collections.`
          )
        console.log(
          `[migrator-live] Collections to migrate: ${validCollections}.`
        )
      }

      const unsubcribers = []

      process.on('SIGINT', function () {
        unsubcribers.forEach(unsubscriber => unsubscriber())
        process.exit()
      })

      for (const collectionName of validCollections) {
        if (debug) {
          console.log(
            `[migrator-live] ${new Date()}: Start listening to ${collectionName} changes.`
          )
        }

        unsubcribers.push(await MIGRATE_LIVE_METHODS[collectionName]())
      }
    } catch (error) {
      console.error(`[migrator-live] Error: `, error)
    }
  })

program.parse(process.argv)
